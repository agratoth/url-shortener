package models

import (
	bongo "github.com/go-bongo/bongo"
)

// Link - model for short links
type Link struct {
	bongo.DocumentBase `json:"-" bson:",inline"`

	URL   string `json:"url" bson:"url"`
	Short string `json:"short" bson:"short"`
	Count int32  `json:"count" bson:"count"`
}

// GetLinkByShort - get object by short id
func GetLinkByShort(short string) (Link, error) {
	var (
		collection = mongo.Collection("links")
		link       = Link{}
		filter     = map[string]interface{}{
			"short": short,
		}
	)

	err := collection.FindOne(filter, &link)

	return link, err
}

// Save - save object to DB
func (link *Link) Save() error {
	var (
		collection = mongo.Collection("links")
	)

	err := collection.Save(link)

	return err
}
