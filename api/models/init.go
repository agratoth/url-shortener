package models

import (
	bongo "github.com/go-bongo/bongo"
)

var (
	mongo *bongo.Connection
)

// InitModels - initialize MongoDB connection
func InitModels(mongoURI string, mongoDB string) error {
	var (
		err error
	)
	config := &bongo.Config{
		ConnectionString: mongoURI,
		Database:         mongoDB,
	}

	mongo, err = bongo.Connect(config)

	return err
}
