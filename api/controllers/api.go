package controllers

import (
	validator "github.com/asaskevich/govalidator"
	gin "github.com/gin-gonic/gin"
	helpers "gitlab.com/agratoth/url-shortener/api/helpers"
	models "gitlab.com/agratoth/url-shortener/api/models"
	http "net/http"
)

func statusHandler(ctx *gin.Context) {
	ctx.JSON(
		http.StatusOK,
		gin.H{
			"status": "ok",
		},
	)
}

func createLink(ctx *gin.Context) {
	var (
		createRequest = createLinkRequest{}
		err           error
	)

	err = ctx.BindJSON(&createRequest)
	valid, _ := validator.ValidateStruct(createRequest)
	if err != nil || !valid {
		ctx.AbortWithStatus(http.StatusBadRequest)
		return
	}

	link := models.Link{
		URL:   createRequest.URL,
		Short: helpers.GenerateString(),
	}
	err = link.Save()
	if err != nil {
		ctx.AbortWithStatus(http.StatusInternalServerError)
		return
	}

	ctx.JSON(
		http.StatusOK,
		link,
	)
}

func resolveLink(ctx *gin.Context) {
	var (
		short = ctx.Param("link")
		link  models.Link
		err   error
	)

	link, err = models.GetLinkByShort(short)
	if err != nil {
		ctx.AbortWithStatus(http.StatusNotFound)
		return
	}

	link.Count++
	link.Save()

	ctx.Redirect(http.StatusTemporaryRedirect, link.URL)
}
