package controllers

type createLinkRequest struct {
	URL string `json:"url" valid:"requrl,required"`
}
