package controllers

import (
	gin "github.com/gin-gonic/gin"
)

// CreateRoutes - create application routes scope
func CreateRoutes(router *gin.Engine) {
	groupAPI := router.Group("/api/v1/links")
	{
		groupAPI.POST("/create", createLink)
	}

	groupMain := router.Group("/api/v1/")
	{
		groupMain.POST("/status", statusHandler)
		groupMain.GET("/status", statusHandler)
	}

	groupResolve := router.Group("/r")
	{
		groupResolve.GET("/:link", resolveLink)
	}
}
