module gitlab.com/agratoth/url-shortener/api

go 1.13

require (
	github.com/asaskevich/govalidator v0.0.0-20200108200545-475eaeb16496
	github.com/getsentry/sentry-go v0.5.1
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.6.2
	github.com/go-bongo/bongo v0.10.4
	github.com/joho/godotenv v1.3.0
	github.com/maxwellhealth/go-dotaccess v0.0.0-20190924013105-74ea4f4ca4eb // indirect
	github.com/oleiade/reflections v1.0.0 // indirect
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22 // indirect
)
