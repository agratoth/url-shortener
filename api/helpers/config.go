package helpers

import (
	"fmt"
	"os"
	"strconv"
	"strings"
)

// Config - config data
type Config struct {
	ServiceHost   string
	ServicePort   string
	Debug         bool
	SentryDSN     string
	MongoURI      string
	MongoDatabase string
	RedisURI      string
}

// New - get config data
func New() *Config {
	return &Config{
		ServiceHost:   getEnv("SERVICE_HOST", "0.0.0.0"),
		ServicePort:   getEnv("SERVICE_PORT", "11001"),
		Debug:         getBool("DEBUG", false),
		SentryDSN:     getEnv("SENTRY_DSN", ""),
		MongoURI:      getEnv("MONGODB_URI", ""),
		MongoDatabase: getEnv("MONGODB_NAME", ""),
		RedisURI:      getEnv("REDIS_URI", ""),
	}
}

// GetServiceSocket - return full socket address
func (config *Config) GetServiceSocket() string {
	return fmt.Sprintf("%s:%s", config.ServiceHost, config.ServicePort)
}

func getEnv(key string, defaultValue string) string {
	if value, exists := os.LookupEnv(key); exists {
		return value
	}

	return defaultValue
}

func getInt(key string, defaultValue int) int {
	valueStr := getEnv(key, "")
	if value, err := strconv.Atoi(valueStr); err == nil {
		return value
	}

	return defaultValue
}

func getBool(key string, defaultValue bool) bool {
	valueStr := getEnv(key, "")
	if value, err := strconv.ParseBool(valueStr); err == nil {
		return value
	}

	return defaultValue
}

func getSlice(key string, defaultValue []string, sep string) []string {
	valStr := getEnv(key, "")
	if valStr == "" {
		return defaultValue
	}

	return strings.Split(valStr, sep)
}
