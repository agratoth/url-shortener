package helpers

import (
	"fmt"
	"math/rand"
	"time"
)

var (
	symbols = "0123456789abcdefghijklmnoprstuvwxyzABCDEFGHIJKLMNOPRSTUVWXYZ"
)

// GenerateString - generate random string
func GenerateString() string {
	var (
		result = ""
	)
	rand.Seed(time.Now().UnixNano())

	for i := 0; i < 7; i++ {
		result = fmt.Sprintf("%s%s", result, string(symbols[rand.Intn(len(symbols))]))
	}

	return result
}
