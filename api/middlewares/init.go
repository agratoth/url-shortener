package middlewares

import (
	gin "github.com/gin-gonic/gin"
)

// InitMiddlewares - initialize all middlewares
func InitMiddlewares(router *gin.Engine) {
	router.Use(headersOverride())
}
