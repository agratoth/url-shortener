package main

import (
	sentry "github.com/getsentry/sentry-go"
	gin "github.com/gin-gonic/gin"
	dotenv "github.com/joho/godotenv"

	controllers "gitlab.com/agratoth/url-shortener/api/controllers"
	helpers "gitlab.com/agratoth/url-shortener/api/helpers"
	middlewares "gitlab.com/agratoth/url-shortener/api/middlewares"
	models "gitlab.com/agratoth/url-shortener/api/models"
)

var (
	router *gin.Engine
	config *helpers.Config
)

func init() {
	dotenv.Load()
	config = helpers.New()

	if !config.Debug {
		gin.SetMode(gin.ReleaseMode)
	}

	sentry.Init(sentry.ClientOptions{
		Dsn: config.SentryDSN,
	})

	router = gin.Default()
	middlewares.InitMiddlewares(router)

	controllers.CreateRoutes(router)

	if err := models.InitModels(config.MongoURI, config.MongoDatabase); err != nil {
		sentry.CaptureException(err)
	}
}

func main() {
	router.Run(config.GetServiceSocket())
}
