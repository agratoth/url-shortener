PROJECT_ROOT := $(shell pwd)
PROJECT := $(notdir $(shell pwd))

.ONESHELL:

# API
clean-api:
	@ echo "Clean API"
	@ rm -f api/build/app api/build/.env

build-api:
	@ echo "Build API"
	@ cd api
	@ go get
	@ CGO_ENABLED=0 go build -o ./build/app ./*.go
	@ cp .env build/

run-api:
	@ echo "Run API"
	@ cd api
	@ ./build/app

rebuild-api: clean-api build-api run-api

build-api-image:
	@ echo "Build API image"
	@ cd api
	@ docker build --rm -t registry.gitlab.com/agratoth/url-shortener:api .

push-api-image:
	@ echo "Push API image"
	@ docker push registry.gitlab.com/agratoth/url-shortener:api

# CLIENT
build-client-image:
	@ echo "Build client image"
	@ cd client
	@ docker build --rm -t registry.gitlab.com/agratoth/url-shortener:client .

push-client-image:
	@ echo "Push client image"
	@ docker push registry.gitlab.com/agratoth/url-shortener:client

# CLUSTER
start-cluster:
	@ echo "Start cluster"
	@ docker-compose up -d

stop-cluster:
	@ echo "Stop cluster"
	@ docker-compose stop

remove-cluster:
	@ echo "Remove cluster"
	@ docker-compose rm -f

reload-cluster: stop-cluster remove-cluster start-cluster