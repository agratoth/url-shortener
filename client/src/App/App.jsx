import React, { Component } from 'react';
import { HashRouter, Route, Switch, Redirect } from 'react-router-dom';

import LinkCreator from '../pages/LinkCreator';

class App extends Component {
  render(){
    return(
      <HashRouter>
        <Switch>
          <Route exact path='/create' render={props => <LinkCreator {...props}/>} />
          <Redirect from='/' to='/create' />
        </Switch>
      </HashRouter>
    );    
  }
}

export default App;