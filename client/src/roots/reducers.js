import { combineReducers } from 'redux';

import { creatorReducer } from '../pages/LinkCreator';


const rootReducer = combineReducers({
  "creator": creatorReducer,
});

export default rootReducer;