import { all } from 'redux-saga/effects';

import { createJATSLinkWatcher } from '../pages/LinkCreator';

export default function* rootSaga() {
  yield all([
    createJATSLinkWatcher(),
  ]);
}