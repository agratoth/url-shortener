import {
  put, takeLatest, delay, select
} from 'redux-saga/effects';

import {
  CREATE_JATSLINK,
  CREATE_JATSLINK_SUCCESS,
  CREATE_JATSLINK_ERROR
} from './constants';

function* createJATSLink() {
  const url = yield select((state) => state.creator.url)
  var error = '';
  var success = false;
  var short = '';

  const response = yield fetch('https://jats.pro/api/v1/links/create', {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    mode: 'cors',
    body: JSON.stringify({url: url})
  });

  switch (response.status) {
    case 400:
      error = "Bad request!"
      break
    case 500:
      error = "Internal error!"
      break
    case 200:
      success = true
      const data = yield response.json()
      short = data.short
      break
    default:
      error = 'Unknown error'
      break
  }

  if (success) {
    yield put({
      type: CREATE_JATSLINK_SUCCESS,
      short: short,
    })
  } else {
    yield put({
      type: CREATE_JATSLINK_ERROR,
      error: error,
    })
  }
}

export default function* createJATSLinkWatcher() {
  yield takeLatest(CREATE_JATSLINK, createJATSLink)
}