import React, { Component } from 'react';
import { connect } from 'react-redux'
import { CopyToClipboard } from 'react-copy-to-clipboard';

import creatorReducer from './reducers';
import createJATSLinkWatcher from './sagas';
import { createJATSLink, updateURL } from './actions';

class LinkCreator extends Component {
  inputChangedHandler = (event) => {
    this.props.updateURL(event.target.value);
  }

  render() {
    const link = `https://jats.pro/r/${ this.props.short }`
    const text = this.props.short.length > 0 ? 'Your short link below' : 'Your short link will be here'
    const result = this.props.short.length > 0 ? 
      <React.Fragment>
        <span>
          <a className="text-dark" href={link} target="_blank">{link}</a>
        </span>
        <CopyToClipboard text={link}>
          <button className="btn btn-outline-dark ml-1 btn-sm">Copy</button>
        </CopyToClipboard>
      </React.Fragment>
      :
      '';

    return(
      <div className="container h-100">
        <div className="row h-100 justify-content-center align-items-center">
          <div className="card text-center" style={{"width": "100%"}}>
            <div className="card-body col-12">
              <h5 className="card-title">Pack Your Link</h5>
              <div className="input-group input-group-lg">
                <input 
                  type="text" 
                  className="form-control"
                  placeholder="http://" 
                  value={ this.props.url } 
                  onChange={(event) => this.inputChangedHandler(event)}/>
              </div>
              <button 
                className="btn btn-success btn-lg mt-3" 
                onClick={this.props.createJATSLink}>
                  Create short link
                </button>
            </div>
            <div className="card-footer text-muted">
              <h5 style={{"height":"70px"}}>
                <p className="mb-2">{ text }</p>
                { result }
              </h5>              
            </div>
          </div>
        </div>        
      </div>     
    );
  }
}

function mapStateToProps(state) {
  return {
    loading: state.creator.loading,
    isError: state.creator.isError,
    errorMessage : state.creator.errorMessage,
    url: state.creator.url,
    short: state.creator.short
  }
}

function mapDispatchToProps(dispatch){
  return {
    createJATSLink: () => {
      dispatch(createJATSLink());
    },
    updateURL: (url) => {
      dispatch(updateURL(url));
    }
  }
}

LinkCreator = connect(mapStateToProps, mapDispatchToProps)(LinkCreator);

export default LinkCreator;
export {
  creatorReducer,
  createJATSLinkWatcher,
}