import { UPDATE_URL, CREATE_JATSLINK } from './constants';

export function createJATSLink(){
  return {
    type: CREATE_JATSLINK,
  }
}

export function updateURL(url){
  return {
    type: UPDATE_URL,
    url: url,
  }
}