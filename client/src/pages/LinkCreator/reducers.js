import {
  UPDATE_URL,
  CREATE_JATSLINK,
  CREATE_JATSLINK_SUCCESS,
  CREATE_JATSLINK_ERROR
} from './constants';

const initState = {
  loading: false,
  isError: false,
  errorMessage : '',
  url: '',
  short: ''
}

const creatorReducer = (state = initState, action) => {
  switch (action.type){
    case UPDATE_URL:
      return {...state, url: action.url}
    case CREATE_JATSLINK:
      return {...state, loading: true}
    case CREATE_JATSLINK_SUCCESS:
      return {...state, loading: false, isError: false, short: action.short}
    case CREATE_JATSLINK_ERROR:
      return {...state, loading: false, isError: true, errorMessage: action.error}
    default:
      return state
  }
}

export default creatorReducer;